package it.ecubit.techtask.tech.configuration;

import org.springframework.context.annotation.Configuration;
import org.springframework.http.MediaType;
import org.springframework.web.servlet.config.annotation.ContentNegotiationConfigurer;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import java.util.Collections;

@Configuration
public class WebConfig implements WebMvcConfigurer {
    @Override
    public void configureContentNegotiation(ContentNegotiationConfigurer configurer) {
        final MediaType mediaType = new MediaType(MediaType.APPLICATION_JSON, Collections.singletonMap("charset", "utf-8"));
        configurer.defaultContentType(mediaType);
    }
}
