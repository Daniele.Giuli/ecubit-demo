package it.ecubit.techtask.tech.web;

import it.ecubit.techtask.tech.dto.CreateQuestDTO;
import it.ecubit.techtask.tech.dto.QuestDTO;
import it.ecubit.techtask.tech.mappers.QuestMapper;
import it.ecubit.techtask.tech.repository.QuestStore;
import it.ecubit.techtask.tech.repository.domain.Quest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
public class QuestResource {

    private final QuestStore questStore;
    private final QuestMapper questMapper;

    public QuestResource(QuestStore questStore, QuestMapper questMapper) {
        this.questStore = questStore;
        this.questMapper = questMapper;
    }

    @GetMapping(value = "/quest/{id}", produces = "application/json")
    public ResponseEntity<QuestDTO> findById(@PathVariable("id") Long id) {
        Optional<Quest> optionalQuest = questStore.getQuest(id);
        if (!optionalQuest.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        Quest quest = optionalQuest.get();
        QuestDTO mapped = questMapper.questToDto(quest);
        return ResponseEntity.ok(mapped);
    }

    @PostMapping(value = "/quest", produces = "application/json", consumes = "application/json")
    public ResponseEntity<QuestDTO> createQuest(@RequestBody CreateQuestDTO createQuestDTO) {
        boolean validationsFailed = ObjectUtils.isEmpty(createQuestDTO.getQuestion()) || createQuestDTO.getAnswers() == null 
        		|| createQuestDTO.getAnswers().length == 0;
        if (validationsFailed) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        Quest q = questStore.setQuest(createQuestDTO.getQuestion(), createQuestDTO.getAnswers());
        QuestDTO mapped = questMapper.questToDto(q);
        return ResponseEntity.ok(mapped);
    }
}
