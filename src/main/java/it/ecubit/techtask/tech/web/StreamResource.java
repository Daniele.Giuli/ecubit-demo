package it.ecubit.techtask.tech.web;

import java.util.NoSuchElementException;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import it.ecubit.techtask.tech.dto.CreateStreamDTO;
import it.ecubit.techtask.tech.dto.StreamDTO;
import it.ecubit.techtask.tech.mappers.StreamMapper;
import it.ecubit.techtask.tech.repository.StreamStore;
import it.ecubit.techtask.tech.repository.domain.Stream;

@RestController
public class StreamResource {

    private final StreamStore streamStore;
    private final StreamMapper streamMapper;

    public StreamResource(StreamStore streamStore, StreamMapper streamMapper) {
        this.streamStore = streamStore;
        this.streamMapper = streamMapper;
    }

    @GetMapping(value = "/stream/{stream_id}", produces = "application/json")
    public ResponseEntity<StreamDTO> findById(@PathVariable("stream_id") Long id) {
        Optional<Stream> optionalStream = streamStore.getStream(id);
        if (!optionalStream.isPresent()) {
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        Stream stream = optionalStream.get();
        StreamDTO mapped = streamMapper.streamToDto(stream);
        return ResponseEntity.ok(mapped);
    }
    
    @GetMapping(value = "/stream", produces = "application/json")
    public ResponseEntity<Page<StreamDTO>> findPaged(
    		@RequestParam(name = "page", required = true) Integer page,
    		@RequestParam(name = "size", required = true) Integer size) {
        Page<Stream> result = streamStore.getStreams(PageRequest.of(page, size));
        return ResponseEntity.ok(result.map(s->streamMapper.streamToDto(s)));
    }

    @PostMapping(value = "/stream", produces = "application/json", consumes = "application/json")
    public ResponseEntity<StreamDTO> createStream(@RequestBody CreateStreamDTO createStreamDTO) {
        boolean validationsFailed = ObjectUtils.isEmpty(createStreamDTO.getStream_name());
        if (validationsFailed) {
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        try {
	        Stream q = streamStore.setStream(createStreamDTO.getStream_name(), createStreamDTO.getQuestions());
	        StreamDTO mapped = streamMapper.streamToDto(q);
	        return ResponseEntity.ok(mapped);
        }catch(NoSuchElementException e) {
        	return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
}
