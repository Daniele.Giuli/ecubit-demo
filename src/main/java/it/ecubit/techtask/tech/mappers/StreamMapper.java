package it.ecubit.techtask.tech.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

import it.ecubit.techtask.tech.dto.StreamDTO;
import it.ecubit.techtask.tech.repository.domain.Quest;
import it.ecubit.techtask.tech.repository.domain.Stream;

@Mapper(componentModel = "spring")
public interface StreamMapper {
	@Mapping(source = "questions", target = "questions", qualifiedByName = "questToId")
    StreamDTO streamToDto(Stream stream);
	
	@Named("questToId") 
    public static Long questToId(Quest quest) { 
        return quest.getId();
    }
}
