package it.ecubit.techtask.tech.mappers;

import it.ecubit.techtask.tech.dto.QuestDTO;
import it.ecubit.techtask.tech.repository.domain.Quest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface QuestMapper {

    QuestDTO questToDto(Quest quest);
}
