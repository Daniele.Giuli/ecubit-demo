package it.ecubit.techtask.tech.dto;

public class CreateStreamDTO {

    private String stream_name;
    private Long[] questions;
    
	public String getStream_name() {
		return stream_name;
	}
	
	public void setStream_name(String stream_name) {
		this.stream_name = stream_name;
	}
	
	public Long[] getQuestions() {
		return questions;
	}
	
	public void setQuestions(Long[] questions) {
		this.questions = questions;
	}

    

}
