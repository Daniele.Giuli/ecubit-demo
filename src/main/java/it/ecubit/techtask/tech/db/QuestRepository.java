package it.ecubit.techtask.tech.db;

import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import it.ecubit.techtask.tech.repository.domain.Quest;

@Profile({"default","native","prod"})
@Repository
public interface QuestRepository extends JpaRepository<Quest, Long>{
	
}
