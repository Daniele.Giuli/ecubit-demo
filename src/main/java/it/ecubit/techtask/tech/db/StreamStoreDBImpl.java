package it.ecubit.techtask.tech.db;

import java.util.Arrays;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import it.ecubit.techtask.tech.repository.QuestStore;
import it.ecubit.techtask.tech.repository.StreamStore;
import it.ecubit.techtask.tech.repository.domain.Quest;
import it.ecubit.techtask.tech.repository.domain.Stream;

@Profile({"default","native","prod"})
@Service
public class StreamStoreDBImpl implements StreamStore {
	
	private Logger logger = LogManager.getLogger(QuestStoreDBImpl.class);

	@Autowired
	StreamRepository streamRepo;
	@Autowired
    private QuestStore questStoreImpl;

    @Override
    public Optional<Stream> getStream(Long id) {
    	logger.info("using db");
        return streamRepo.findById(id);
    }

    @Override
    public Stream setStream(String streamName, Long[] questions) {
    	logger.info("using db");
        Stream s = new Stream();
        s.setStream_name(streamName);
        if(questions!=null) {
        	s.setQuestions(
        			Arrays.stream(questions)
        			.map((id)->{
        				Optional<Quest> opt = questStoreImpl.getQuest(id);
        				if(opt.isPresent()) {
        					return opt.get();
        				}else {
        					throw new NoSuchElementException();
        				}
        			}).collect(Collectors.toList()));
        }
        return streamRepo.save(s);
    }

	@Override
	public Page<Stream> getStreams(Pageable pageable) {
		logger.info("using db");
		return streamRepo.findAll(pageable);
	}
}
