package it.ecubit.techtask.tech.db;

import java.util.Optional;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import it.ecubit.techtask.tech.repository.QuestStore;
import it.ecubit.techtask.tech.repository.domain.Quest;

@Profile({"default","native","prod"})
@Service
public class QuestStoreDBImpl implements QuestStore {
	
	private Logger logger = LogManager.getLogger(QuestStoreDBImpl.class);
	
	@Autowired
	private QuestRepository questRepo;


    @Override
    public Optional<Quest> getQuest(Long id) {
    	logger.info("using db");
    	return questRepo.findById(id);
    }

    @Override
    public Quest setQuest(String question, String[] answers) {
    	logger.info("using db");
        Quest q = new Quest();
        q.setQuestion(question);
        q.setAnswers(answers);
        return questRepo.save(q);
    }
}
