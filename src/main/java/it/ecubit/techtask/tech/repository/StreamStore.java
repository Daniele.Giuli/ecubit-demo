package it.ecubit.techtask.tech.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import it.ecubit.techtask.tech.repository.domain.Stream;

public interface StreamStore {
    Optional<Stream> getStream(Long id);
    Stream setStream(String stream, Long[] questions);
	Page<Stream> getStreams(Pageable pageable);
}
