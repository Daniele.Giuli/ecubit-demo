package it.ecubit.techtask.tech.repository;

import it.ecubit.techtask.tech.repository.domain.Quest;

import java.util.Optional;

public interface QuestStore {
    Optional<Quest> getQuest(Long id);
    Quest setQuest(String question, String[] answers);
}
