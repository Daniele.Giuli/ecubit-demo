package it.ecubit.techtask.tech.repository.domain;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="streams")
public class Stream {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String stream_name;
	@OneToMany
	@JoinColumn(name="stream_id")
    private List<Quest> questions;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getStream_name() {
		return stream_name;
	}
	
	public void setStream_name(String stream_name) {
		this.stream_name = stream_name;
	}

	public List<Quest> getQuestions() {
		return questions;
	}

	public void setQuestions(List<Quest> questions) {
		this.questions = questions;
	}
	

}
