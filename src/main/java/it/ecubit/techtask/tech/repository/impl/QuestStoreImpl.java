package it.ecubit.techtask.tech.repository.impl;

import it.ecubit.techtask.tech.repository.QuestStore;
import it.ecubit.techtask.tech.repository.domain.Quest;

import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;

@Profile({"dev"})
@Repository
public class QuestStoreImpl implements QuestStore {

    private final AtomicLong dbId = new AtomicLong(0L);
    private final Map<Long, Quest> questStoreMap = new LinkedHashMap<>();


    @Override
    public Optional<Quest> getQuest(Long id) {
        if (!questStoreMap.containsKey(id)) {
            return Optional.empty();
        }
        return Optional.of(questStoreMap.get(id));
    }

    @Override
    public Quest setQuest(String question, String[] answers) {
        Long currId = dbId.incrementAndGet();
        Quest q = new Quest();
        q.setId(currId);
        q.setQuestion(question);
        q.setAnswers(answers);
        questStoreMap.put(currId, q);
        return q;
    }
}
