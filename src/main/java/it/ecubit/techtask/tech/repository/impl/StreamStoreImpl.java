package it.ecubit.techtask.tech.repository.impl;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Repository;

import it.ecubit.techtask.tech.repository.QuestStore;
import it.ecubit.techtask.tech.repository.StreamStore;
import it.ecubit.techtask.tech.repository.domain.Quest;
import it.ecubit.techtask.tech.repository.domain.Stream;

@Profile({"dev"})
@Repository
public class StreamStoreImpl implements StreamStore {

    private final AtomicLong dbId = new AtomicLong(0L);
    private final Map<Long, Stream> streamStoreMap = new LinkedHashMap<>();
    @Autowired
    private QuestStore questStoreImpl;

    @Override
    public Optional<Stream> getStream(Long id) {
        if (!streamStoreMap.containsKey(id)) {
            return Optional.empty();
        }
        return Optional.of(streamStoreMap.get(id));
    }

    @Override
    public Stream setStream(String streamName, Long[] questions) {
        Long currId = dbId.incrementAndGet();
        Stream s = new Stream();
        s.setId(currId);
        s.setStream_name(streamName);
        if(questions!=null) {
        	s.setQuestions(
        			Arrays.stream(questions)
        			.map((id)->{
        				Optional<Quest> opt = questStoreImpl.getQuest(id);
        				if(opt.isPresent()) {
        					return opt.get();
        				}else {
        					throw new NoSuchElementException();
        				}
        			}).collect(Collectors.toList()));
        }
        streamStoreMap.put(currId, s);
        return s;
    }

	@Override
	public Page<Stream> getStreams(Pageable pageable) {
		Collection<Stream> values = streamStoreMap.values();
		final int start = (int)pageable.getOffset();
		final int end = Math.min((start + pageable.getPageSize()), values.size());		
		return new PageImpl<>(values.stream().collect(Collectors.toList())
				.subList(start, end), pageable, values.size());
	}
}
