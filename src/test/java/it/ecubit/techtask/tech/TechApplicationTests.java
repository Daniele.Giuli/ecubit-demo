package it.ecubit.techtask.tech;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;

import it.ecubit.techtask.tech.dto.CreateQuestDTO;
import it.ecubit.techtask.tech.dto.CreateStreamDTO;
import it.ecubit.techtask.tech.dto.QuestDTO;
import it.ecubit.techtask.tech.dto.StreamDTO;
import it.ecubit.techtask.tech.web.QuestResource;
import it.ecubit.techtask.tech.web.StreamResource;

@SpringBootTest()
class TechApplicationTests {
	
	private Logger logger = LogManager.getLogger(TechApplicationTests.class);

	@Autowired
	private QuestResource questResource;
	@Autowired
	private StreamResource streamResource;
	
	private Long questionId;
	private Long streamId;
	
    @Test
    void contextLoads() {
    	logger.info("Testing application ... ");
    }
    
    @Test 
    void testQuestions(){
    	logger.info("Testing question resource ... ");
    	questionInsert();
    	questionGet();
    	logger.info("Question resource ok");
    }
    
    @Test 
    void testStreams(){
    	logger.info("Testing stream resource ... ");
    	streamInsert();
    	questionInsert();
    	streamInsertWithQuestions();
    	streamGet();
    	streamGetPaged();
    	logger.info("Stream resource ok");
    }

	void questionInsert() {
    	CreateQuestDTO newQuestion = new CreateQuestDTO();
    	newQuestion.setQuestion("Does this work?");
    	newQuestion.setAnswers(new String[]{"Yes","No","Maybe","I don't know","Can you repeat the question?"});
    	ResponseEntity<QuestDTO> resp = questResource.createQuest(newQuestion);
    	Assert.isTrue(HttpStatus.OK.equals(resp.getStatusCode()),"testQuestionInsert fail");
    	Assert.isTrue(resp.getBody()!=null,"testQuestionInsert fail");
    	questionId = resp.getBody().getId();
    }

    void questionGet() {
    	ResponseEntity<QuestDTO> resp = questResource.findById(questionId);
    	Assert.isTrue(HttpStatus.OK.equals(resp.getStatusCode()),"testQuestionGet fail");
    	Assert.isTrue(resp.getBody()!=null,"testQuestionGet fail");
    	Assert.isTrue(resp.getBody().getId().equals(questionId),"testQuestionGet fail");
    }

    void streamInsert() {
    	CreateStreamDTO newStream = new CreateStreamDTO();
    	newStream.setStream_name("Does this work?");
    	ResponseEntity<StreamDTO> resp = streamResource.createStream(newStream);
    	Assert.isTrue(HttpStatus.OK.equals(resp.getStatusCode()),"testStreamInsert fail");
    	Assert.isTrue(resp.getBody()!=null,"testStreamInsert fail");
    	streamId = resp.getBody().getId();
    }
    

    private void streamInsertWithQuestions() {
    	CreateStreamDTO newStream = new CreateStreamDTO();
    	newStream.setStream_name("Does this work?");
    	newStream.setQuestions(new Long[] {questionId});
    	ResponseEntity<StreamDTO> resp = streamResource.createStream(newStream);
    	Assert.isTrue(HttpStatus.OK.equals(resp.getStatusCode()),"testStreamInsert fail");
    	Assert.isTrue(resp.getBody()!=null,"testStreamInsert fail");
    	Assert.isTrue(resp.getBody().getQuestions()!=null
    			&&resp.getBody().getQuestions().length==1
    			&&resp.getBody().getQuestions()[0]==questionId,"testStreamInsert fail");
    	streamId = resp.getBody().getId();
		
	}

    void streamGet() {
    	ResponseEntity<StreamDTO> resp = streamResource.findById(streamId);
    	Assert.isTrue(HttpStatus.OK.equals(resp.getStatusCode()),"testStreamGet fail");
    	Assert.isTrue(resp.getBody()!=null,"testStreamGet fail");
    	Assert.isTrue(resp.getBody().getId().equals(streamId),"testStreamGet fail");
    }

    void streamGetPaged() {
    	ResponseEntity<Page<StreamDTO>> resp = streamResource.findPaged(0, 10);
    	Assert.isTrue(HttpStatus.OK.equals(resp.getStatusCode()),"testStreamGetPaged fail");
    	Assert.isTrue(resp.getBody()!=null,"testStreamGetPaged fail");
    	Assert.isTrue(!resp.getBody().isEmpty(),"testStreamGetPaged fail");
    }
    
    

}
